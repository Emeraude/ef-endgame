# Eternalfest game result viewer

[https://gameresult.ef.rainbowda.sh/](https://gameresult.ef.rainbowda.sh)

```sh
npm ci
mv config.json.default config.json
$EDITOR config.json
env PORT=80 npm start
```
