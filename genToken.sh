#!/usr/bin/env bash

if [ "$#" -lt 2 ]; then
    echo "Usage: $0 username password [server]" && exit 1
fi

username=$1
password=$(echo -n $2 | xxd -p)
server=${3-"fr"}

curl --header "Content-Type: application/json" --request PUT --data '{"password":"'$password'", "username":"'$username'", "server":"'$server'"}' "https://eternalfest.net/api/v1/self/session?strategy=hfest-credentials" -i 2>&- | grep sid | cut -d= -f2 | cut -d';' -f1
