const fetch = require('node-fetch')
const HEADERS= {
  "headers": {
    "Cookie": "ef_sid=" + config.authToken
  }
}

module.exports.getFullGame = async function(run_id) {
  const req = await fetch(config.eternalfestUrl + '/api/v1/runs/' + run_id, HEADERS)
  const json = await req.json()
  return { status: req.status, json }
}

module.exports.getGame = async function(game_id) {
  const req = await fetch(config.eternalfestUrl + '/api/v1/games/' + game_id, HEADERS)
  const json = await req.json()
  return { status: req.status, json }
}

module.exports.getIconPath = async function(game_id) {
  let req = await fetch(config.eternalfestUrl + '/api/v1/games/' + game_id, HEADERS)
  const json = await req.json()
  return '/api/v1/blobs/' + json.channels.active.build.icon.id + '/raw'
}
