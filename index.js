#!/usr/bin/env node

global.config = require('./config.json')
const path = require('path')
const express = require('express')
const i18n = require('i18n')
const requests = require('./requests.js')
const tools = require('./tools.js')
const fs = require("fs")

i18n.configure({
  locales: ['fr', 'en', 'es'],
  queryParameter: 'lang',
  indent: '  ',
  directory: path.join(__dirname, 'locales')
})

const app = express()

app.set('view engine', 'pug')
app.set('x-powered-by', false)

const PORT = process.env.PORT || config.port || 3000
app.listen(PORT, (err) => {
  console.log(`Listening on port ${PORT}.`)
})
app.use(express.static('./public'))
app.use(i18n.init)

app.use(function(req, res, next) {
  res.locals.req = req
  res.locals.placeholder = config.placeholder || ""
  res.locals.eternalfestUrl = config.eternalfestUrl
  next()
})

app.get("/list", async (req, res) => {
  let games = [];
  fs.readdir("./database", (err, files) => {
    for(const db of files) {
      if (db.endsWith(".json")) {
        for (const game of require(`./database/${db}`)) {
          games.push(game)
        }
      }
    }

    listGameresults(req, res, games)
  })
})

app.get('/:run_id', async function(req, res) {
  try {
    const { status, json } = await requests.getFullGame(req.params.run_id)
    if (status === 404 || status === 422) {
      res.status(404).render('notfound', {game: {run_id: req.params.run_id}})
    } else if (status === 500) {
      res.status(500).render('error', json)
    } else if (status !== 200) {
      res.status(status).render(error, json)
    } else {
      game_res = (await requests.getGame(json.game.id)).json
      json.game.display_name = game_res.channels.active.build.display_name
      json.game.icon = config.eternalfestUrl + '/api/v1/blobs/' + game_res.channels.active.build.icon.id + '/raw'
      const results = tools.addResults(json)
      save(results)
      res.render('result', {game: results})
    }
  } catch(e) {
    console.error(e)
    res.status(500).render('error')
  }
})

app.get("/list/:game_id", async (req, res) => {
  let db = undefined;
  try {
    db = require(getDBPath(req.params.game_id));
  } catch(e) {}

  listGameresults(req, res, db, req.params.game_id)
})

function listGameresults(req, res, games, land_id) {
  switch(req.query.sort) {
    case "alphabetical":
      games = games.sort((r1, r2) => r1.user.display_name.localeCompare(r2.user.display_name));
      break;
    case "score":
      games = games.sort((r1, r2) => r2.result.scores.reduce((acc, elem) => acc + elem) - r1.result.scores.reduce((acc, elem) => acc + elem));
      break;
    case "duration":
      games = games.sort((r1, r2) => r2.result.duration.milliseconds - r1.result.duration.milliseconds);
      break;
    case "newest":
      games = games.sort((r1, r2) => new Date(r2.started_at) - new Date(r1.started_at));
      break;
    case "land_name":
      games = games.sort((r1, r2) => r1.game.display_name.localeCompare(r2.game.display_name));
      break;
  }

  res.render("list", {games, land_id})
}

app.get('/', function(req, res) {
  if (req.query.id) {
    const matchURL = config.eternalfestUrl + "/runs/"
    if (req.query.id.slice(0, matchURL.length) === matchURL)
      req.query.id = req.query.id.slice(matchURL.length)
    res.redirect(req.query.id)
  } else {
    res.render('home')
  }
})

app.get('*', function(req, res) {
  res.status(404).render('notfound')
})

if (config.samples) {
  app.get('/sample/:name', function(req, res) {
    try {
      let game = require('./samples/' + req.params.name)
      game = tools.addResults(game)
      res.render('result', {game: game})
    } catch (e) {
      res.status(404).render('notfound', {game: {run_id: req.params.name}})
    }
  })
}

function getDBPath(game_id) {
  return `./database/${game_id}.json`
}

async function save(json) {
  const DB_PATH = getDBPath(json.game.id)

  // Creates and write [] in the database file if it doesn't exist yet
  try {
    await fs.promises.writeFile(DB_PATH, "[]", {"flag": "wx"});
  } catch(e) {
    console.info(`Created new database for land \`${json.game.display_name}\``)
  }

  const db = require(DB_PATH)
  if (! db.find(game => game.id === json.id)) {
    db.push(json)
    fs.promises.writeFile(DB_PATH, JSON.stringify(db))
  }
}
