function durationToString(ms) {
  const totalSeconds = ms / 1000
  const displaySeconds = parseInt(totalSeconds % 60)
  const displayMinutes = parseInt((totalSeconds / 60) % 60)
  const displayHours = parseInt(totalSeconds / 3600)
  let string = ""
  if (displayHours > 0)
    string += displayHours + "h"
  if (displayMinutes > 0)
    string += displayMinutes + "m"
  if (displaySeconds > 0)
    string += displaySeconds + "s"
  return string
}

function addResults(json) {
  if (json.result && json.result.scores) {
    json.result.created_at = new Date(json.result.created_at)
    json.started_at = new Date(json.started_at)
    json.result.duration = {
      milliseconds: json.result.created_at - json.started_at
    }
    json.result.duration.string = durationToString(json.result.duration.milliseconds)
    json.result.display_score = json.result.scores.reduce(((a, b) => a + b), 0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
    json.result.display_scores = json.result.scores.map(s => s.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."))
  }
  return json
}

module.exports.durationToString = durationToString
module.exports.addResults = addResults
